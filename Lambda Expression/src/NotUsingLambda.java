import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NotUsingLambda {
	

	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("Sujan", "Ghimire", 27),
				new Person("Duong", "Vu", 25),
				new Person("Almaz", "Egga", 21),
				new Person("Peter", "Graper", 29)
				);
		//Step 1: Sort List by Last Name
		
		Collections.sort(people, new Comparator<Person>(){
			
			
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
				
			}
			
		});
		
		
		//Step 2: Create a prints all elements in the list
		
		printall(people);
		
		//Step 3: Create a method that prints name with last name starting with  G
		printByLastNameG(people);
		
	}

	private static void printByLastNameG(List<Person> people) {
		for(Person p: people) {
			if(p.getLastName().startsWith("G")) {
			System.out.println(p);
			}
		}
	}

	private static void printall(List<Person> people) {
		
		for(Person p: people) {
			System.out.println(p);
		}
	}

}
